# Test of PDF Artifacts
This is an example of using GitLab CI/CD to generate a PDF file from a LaTeX source.
The artifacts are saved forever unless you set them to expire

See `.gitlab-ci.yml` for details
